package com.safebear.app;

import com.safebear.app.Utils.Utils;
import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 19/07/2018.
 */
public class BaseTest {

    WebDriver driver;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    Utils utility;

    @Before
    public void setUp() {
        utility = new Utils();
        driver = utility.getDriver();

        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);


        driver.get(utility.getUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown () {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }
}
