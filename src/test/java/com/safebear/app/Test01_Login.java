package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 19/07/2018.
 */
public class Test01_Login extends BaseTest
{
    @Test
    public void testLogin () {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 click on the Login link and the Login Page loads
        welcomePage.clickLoginLink();
        //Step 3 confirm that we're now on the Login page
        assertTrue(loginPage.checkCorrectPage());
//Step 4 Login with valid credentials
        loginPage.login("testuser","testing");
        //Step 5 check that we're now on the User Page
        assertTrue(userPage.checkCorrectPage());
    }
}
