package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 19/07/2018.
 */
public class UserPage {

    WebDriver driver;


    public UserPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Boolean checkCorrectPage() {
        return driver.getTitle().contains("Logged In");
    }
}

