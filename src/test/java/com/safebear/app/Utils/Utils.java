package com.safebear.app.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by CCA_Student on 19/07/2018.
 */
public class Utils {
    private WebDriver driver;
    private String  url;
    private String browser;
    Properties prop = new Properties ();
    InputStream input = null;

            public Utils () {
                try {
                    String File = "config.properties";
                    input = Utils.class.getClassLoader().getResourceAsStream(File);
                    if (input == null) {
                        System.out.println("Unable to find" + File);
                        return;
                    }
                    prop.load(input);
                    this.url = prop.getProperty("url");
                    this.browser = prop.getProperty("browser");

                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                switch (browser) {
                    case "chrome":
                        this.driver = new ChromeDriver();
                        break;
                    case "chrome_headless":
                        ChromeOptions chromeOptions = new ChromeOptions();
                        chromeOptions.addArguments("headless");
                        this.driver = new ChromeDriver(chromeOptions);
                        break;
                    default:
                        this.driver = new ChromeDriver();
                        break;


                }
            }



    public WebDriver getDriver () {
        return driver;
    }
    public String getUrl () {return url;}
}
